CREATE DATABASE practica2_FiestaSA;
USE fiestaSA;

CREATE TABLE tipo(

    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(100) NOT NULL

);

CREATE TABLE material(

    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(100) NOT NULL

);

CREATE TABLE color(

    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(100) NOT NULL

);

CREATE TABLE rol(

    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(100) NOT NULL

);

CREATE TABLE usuario(

    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    contrasenia VARCHAR(100) NOT NULL,
    rol INT NOT NULL,
    FOREIGN KEY (rol) REFERENCES rol(codigo)

);

CREATE TABLE producto (

    codigo INT PRIMARY KEY NOT NULL,
    descripcion VARCHAR(300) NOT NULL,
    cantidadDisponible INT NOT NULL,
    costoPorUnidad FLOAT NOT NULL,
    codigoUsuario INT NOT NULL,
    codigoTipo INT NOT NULL,
    codigoMaterial INT NOT NULL,
    codigoColor INT NOT NULL,
    FOREIGN KEY (codigoUsuario) REFERENCES usuario(codigo),
    FOREIGN KEY (codigoTipo) REFERENCES tipo(codigo),
    FOREIGN KEY (codigoMaterial) REFERENCES material(codigo),
    FOREIGN KEY (codigoColor) REFERENCES color(codigo)

);

 /*
        +----------------------------------------------------
        |         INSERCIÓN DE DATOS A LA TABLA TIPO
        +----------------------------------------------------
 
 */

INSERT INTO tipo VALUES (1,"Sillas");
INSERT INTO tipo VALUES (2,"Mesas");
INSERT INTO tipo VALUES (3,"Cucharas");
INSERT INTO tipo VALUES (4,"Tenedores");
INSERT INTO tipo VALUES (5,"Cuchillos");
INSERT INTO tipo VALUES (6,"Servilletas");
INSERT INTO tipo VALUES (7,"Manteles");
INSERT INTO tipo VALUES (8,"Plato Ondo");
INSERT INTO tipo VALUES (9,"Plato Plano");
INSERT INTO tipo VALUES (10,"Plato Pastelero");

 /*
        +----------------------------------------------------
        |         INSERCIÓN DE DATOS A LA TABLA MATERIA
        +----------------------------------------------------
 
 */


INSERT INTO material VALUES (1,"Plastico");
INSERT INTO material VALUES (2,"Aluminio");
INSERT INTO material VALUES (3,"Metal");
INSERT INTO material VALUES (4,"Madera");
INSERT INTO material VALUES (5,"Ceramica");
INSERT INTO material VALUES (6,"Poliester");
INSERT INTO material VALUES (7,"Algodon");
INSERT INTO material VALUES (8,"Satén");

 /*
        +----------------------------------------------------
        |         INSERCIÓN DE DATOS A LA TABLA COLOR
        +----------------------------------------------------
 
 */

INSERT INTO color VALUES (1,"Blanco");
INSERT INTO color VALUES(2,"Negro");
INSERT INTO color VALUES(3,"Rojo");
INSERT INTO color VALUES(4,"Amarillo");
INSERT INTO color VALUES(5,"Azul");
INSERT INTO color VALUES(6,"Anaranjado");
INSERT INTO color VALUES(7,"Morado");
INSERT INTO color VALUES(8,"Verde");
INSERT INTO color VALUES(9,"Cafe");
INSERT INTO color VALUES(10,"Gris");
INSERT INTO color VALUES(11,"Beige");
INSERT INTO color VALUES(12,"Plateado");
INSERT INTO color VALUES(13,"Dorado");

 /*
        +----------------------------------------------------
        |         INSERCIÓN DE DATOS A LA TABLA ROL
        +----------------------------------------------------
 
 */

INSERT INTO rol VALUES (1,"administrador");
INSERT INTO rol VALUES (2,"empleado");

 /*
        +----------------------------------------------------
        |         INSERCIÓN DE DATOS A LA TABLA USUARIO
        +----------------------------------------------------
 
 */

INSERT INTO usuario VALUES (1,"admin","admin",1);
INSERT INTO usuario VALUES (2,"empleado","empleado",2);

 /*
        +----------------------------------------------------
        |         INSERCIÓN DE DATOS A LA TABLA PRODUCTO
        +----------------------------------------------------
 
 */

INSERT INTO producto VALUES (1,"Banco Individual",45,15,1,1,4,1);
INSERT INTO producto VALUES (2,"Tablero Rectangular",24,200,1,2,1,1);
INSERT INTO producto VALUES (3,"Tablero Redondo",15,150,2,2,1,1);
INSERT INTO producto VALUES (4,"Silla Individual Tipo 1",40,25,2,1,3,12);
INSERT INTO producto VALUES (5,"Silla Individual Tipo 2",43,25,1,1,1,1);
INSERT INTO producto VALUES (6,"Mantel de dimension 2x1 mts",35,20,2,6,6,1);

